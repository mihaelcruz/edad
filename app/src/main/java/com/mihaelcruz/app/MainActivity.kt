package com.mihaelcruz.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener{
            val edad= edtEdad.text.toString().toInt()

            tvResultado.text = when(edad){
                in 1..17 -> {
                    "Usted es menor de edad"
                }
                in 18..110 -> {
                    "Usted es mayor de edad"
                }
                else->{
                    "Usted ingreso una edad no valida"
                }
            }
        }

    }
}